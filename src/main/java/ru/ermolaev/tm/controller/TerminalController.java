package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.constant.ITerminalConst;
import ru.ermolaev.tm.constant.IArgumentConst;
import ru.ermolaev.tm.model.TerminalCommand;
import ru.ermolaev.tm.util.NumberUtil;

public class TerminalController {

    private TerminalController () {}

    public static void parseArg(final String arg) {
        if (arg.isEmpty()) return;
        switch (arg) {
            case IArgumentConst.HELP:
                showHelp();
                break;
            case IArgumentConst.ABOUT:
                showAbout();
                break;
            case IArgumentConst.VERSION:
                showVersion();
                break;
            case IArgumentConst.INFO:
                showInfo();
                break;
            default:
                System.out.println("Invalid argument");
        }
    }

    public static void parseCommand(final String arg) {
        if (arg.isEmpty()) return;
        switch (arg) {
            case ITerminalConst.HELP:
                showHelp();
                break;
            case ITerminalConst.ABOUT:
                showAbout();
                break;
            case ITerminalConst.VERSION:
                showVersion();
                break;
            case ITerminalConst.INFO:
                showInfo();
                break;
            case ITerminalConst.EXIT:
                exit();
            default:
                System.out.println("Invalid command");
        }
    }

    public static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalCommand.ABOUT);
        System.out.println(TerminalCommand.VERSION);
        System.out.println(TerminalCommand.HELP);
        System.out.println(TerminalCommand.INFO);
        System.out.println(TerminalCommand.EXIT);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Evgeniy Ermolaev");
        System.out.println("E-MAIL: ermolaev.evgeniy.96@yandex.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.8");
    }

    private static void showInfo() {
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(Runtime.getRuntime().freeMemory()));
        System.out.println("Maximum memory: " + (Runtime.getRuntime().maxMemory() == Long.MAX_VALUE ? "no limit" : NumberUtil.formatBytes(Runtime.getRuntime().maxMemory())));
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()));
    }

    private static void exit() {
        System.exit(0);
    }

}
